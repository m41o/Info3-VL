package info3.vl.observer;

public class Pkw extends Kfz {
  
  private Werkstatt ws;
  private int seats;
  
  public Pkw(Werkstatt w, int bj, int s) {
    super(bj);
    ws = w;
    seats = s;
  }
  
  public int getSeats() {
    return seats;
  }
  
  public boolean isOldtimer() {
    if (2018 - this.getBaujahr() >= 30)
      return true;
    else
      return false;
  }
  
  public void takeAction() {
    ws.getAppointment();
  }
}
