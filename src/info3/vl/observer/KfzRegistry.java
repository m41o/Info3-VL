package info3.vl.observer;

import java.util.ArrayList;

public abstract class KfzRegistry {
  
  private ArrayList<Kfz> kfzs = new ArrayList<Kfz>();
  
  public void register(Kfz k) {
    kfzs.add(k);
    System.out.println("Fahrzeug registriert.");
  }
  
  public void unregister(Kfz k) {
    kfzs.remove(k);
    System.out.println("Fahrzeug entfernt.");
  }
  
  public void inform() {
    for (Kfz kfz : kfzs) {
      if (kfz != null)
        kfz.takeAction();
    }
  }
}
