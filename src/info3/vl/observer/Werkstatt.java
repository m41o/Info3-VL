package info3.vl.observer;

public class Werkstatt extends KfzRegistry {
  public static void main(String[] args) {
    
    Werkstatt ws = new Werkstatt();
    Pkw pkw = new Pkw(ws, 1984, 4);
    Lkw lkw = new Lkw(ws, 2005, 5000);
    
    ws.register(pkw);
    ws.register(lkw);
    
    ws.inform();
    
    ws.unregister(pkw);
    ws.unregister(lkw);
  }
  
  public int getAppointment() {
    System.out.println("Termin vereinbart.");
    
    return 0;
  }
  
  public String getInfo() {
    return "Infos aus Werkstatt.";
  }
}
