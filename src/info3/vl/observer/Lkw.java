package info3.vl.observer;

public class Lkw extends Kfz {
  
  private Werkstatt ws;
  private int load;
  
  public Lkw(Werkstatt w, int bj, int l) {
    super(bj);
    ws = w;
    load = l;
  }
  
  public int getLoad() {
    return load;
  }
  
  public void takeAction() {
    System.out.println(ws.getInfo());
  }
}
