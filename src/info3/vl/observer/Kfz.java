package info3.vl.observer;

public abstract class Kfz {
  
  private int baujahr;
  private String plate;
  
  public Kfz(int baujahr) {
    this.baujahr = baujahr;
  }
  
  public int getBaujahr() {
    return baujahr;
  }
  
  public String getPlate() {
    return plate;
  }
  
  public void setPlate(String p) {
    plate = p;
  }
  
  public abstract void takeAction();
}
