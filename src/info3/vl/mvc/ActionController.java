package info3.vl.mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionController implements ActionListener {
  private CounterModel cModel;
  
  public ActionController(CounterModel cModel) {
    this.cModel = cModel;
  }
  
  public void actionPerformed(ActionEvent arg0) {
    cModel.setCounter(cModel.getCounter() + 1);
    cModel.inform();
  }
}
