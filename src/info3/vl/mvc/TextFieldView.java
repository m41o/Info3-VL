package info3.vl.mvc;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class TextFieldView extends JFrame implements CounterListener{
  private JTextField inTextField;
  private JTextField outTextField;
  private CounterModel cModel;
  
  public TextFieldView(CounterModel cModel) {
    super("TextFieldView");
    
    this.cModel = cModel;
    cModel.register(this);
    
    outTextField = new JTextField();
    outTextField.setText("Counter: " + cModel.getCounter());
    outTextField.setEditable(false);
    
    inTextField = new JTextField();
    inTextField.addActionListener(new ActionController(cModel));
    
    this.add(outTextField, BorderLayout.SOUTH);
    this.add(inTextField, BorderLayout.NORTH);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
  }
  
  @Override
  public void update() {
    outTextField.setText("Counter: " + cModel.getCounter());
  }
}
