package info3.vl.mvc;

import java.util.LinkedList;

public class Registry {
  private LinkedList<CounterListener> listeners;
  
  public Registry() {
    listeners = new LinkedList<CounterListener>();
  }
  
  public void unregister(CounterListener cL) {
    listeners.remove(cL);
  }
  
  public void register(CounterListener cL) {
    listeners.add(cL);
  }
  
  public void inform() {
    for (CounterListener cL : listeners)
      cL.update();
  }
}
