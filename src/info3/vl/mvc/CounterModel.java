package info3.vl.mvc;

public class CounterModel extends Registry {
  private int counter = 0;
  
  public int getCounter() {
    return counter;
  }
  
  public void setCounter(int c) {
    counter = c;
  }
}
