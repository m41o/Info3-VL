package info3.vl.mvc;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ButtonView extends JFrame implements CounterListener {
  private JButton button;
  private CounterModel cModel;
  
  public ButtonView(CounterModel cModel) {
    super("ButtonView");
    
    this.cModel = cModel;
    cModel.register(this);
    
    button = new JButton();
    button.addActionListener(new ActionController(cModel));
    button.setText(Integer.toString(cModel.getCounter()));
    
    this.add(button);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
  }
  
  @Override
  public void update() {
    button.setText(Integer.toString(cModel.getCounter())); 
  }

}
