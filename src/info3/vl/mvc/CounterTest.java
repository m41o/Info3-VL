package info3.vl.mvc;

public class CounterTest {

  public static void main(String[] args) {
    CounterModel cModel = new CounterModel();
    TextFieldView tV = new TextFieldView(cModel);
    ButtonView bV = new ButtonView(cModel);
    
    tV.setSize(300, 100);
    tV.setVisible(true);
    bV.setSize(300, 100);
    bV.setVisible(true);
  }
}
