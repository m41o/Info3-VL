package info3.vl.interfaces;

public class Lkw implements KfzListener {
  
  private Werkstatt ws;
  private int baujahr;
  
  public Lkw(Werkstatt w, int bj) {
    ws = w;
    baujahr = bj;
  }
  
  public int getBaujahr() {
    return baujahr;
  }
  
  public void takeAction() {
    System.out.println(ws.getInfo());
  }
}
