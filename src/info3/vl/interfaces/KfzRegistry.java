package info3.vl.interfaces;

import java.util.ArrayList;

public abstract class KfzRegistry {
  
  private ArrayList<KfzListener> kfzs = new ArrayList<KfzListener>();
  
  public void register(KfzListener k) {
    kfzs.add(k);
    System.out.println("Fahrzeug registriert.");
  }
  
  public void unregister(KfzListener k) {
    kfzs.remove(k);
    System.out.println("Fahrzeug entfernt.");
  }
  
  public void inform() {
    for (KfzListener kfz : kfzs) {
      if (kfz != null)
        kfz.takeAction();
    }
  }
}
