package info3.vl.interfaces;

public class Werkstatt extends KfzRegistry {
  public static void main(String[] args) {
    
    Werkstatt ws = new Werkstatt();
    Pkw pkw = new Pkw(ws, 1984);
    Lkw lkw = new Lkw(ws, 2005);
    
    Pkw pkw2 = new Pkw(ws, 1984);
    Pkw pkw3 = pkw;
    
    if(pkw2.equals(pkw))
      System.out.println("pkw2 == pkw");
    
    if(pkw3.equals(pkw))
      System.out.println("pkw3 == pkw");
    
    System.out.println(pkw.toString());
    System.out.println(pkw2.toString());
    
    ws.register(pkw);
    ws.register(lkw);
    
    ws.inform();
    
    ws.unregister(pkw);
    ws.unregister(lkw);
  }
  
  public int getAppointment() {
    System.out.println("Termin vereinbart.");
    
    return 0;
  }
  
  public String getInfo() {
    return "Infos aus Werkstatt.";
  }
}
