package info3.vl.interfaces;

public class Pkw implements KfzListener {
  
  private Werkstatt ws;
  private int baujahr;
  
  public Pkw(Werkstatt w, int bj) {
    ws = w;
    baujahr = bj;
  }
  
  public int getBaujahr() {
    return baujahr;
  }
  
  public boolean isOldtimer() {
    if (2018 - this.getBaujahr() >= 30)
      return true;
    else
      return false;
  }
  
  public void takeAction() {
    ws.getAppointment();
  }
  
  public String toString() {
    return "Werkstatt: " + ws.toString() + "\nBaujahr: " + baujahr;
  }
  
  public boolean equals(Object obj) {
    if (obj instanceof Pkw) {
      Pkw p = (Pkw) obj;
      if(p.ws == this.ws && p.baujahr == this.baujahr)
        return true;
     } 
     
    return false;
  }
}
