package info3.vl.singleton;
public class Werkstatt {
  public static void main(String[] args) {
    Motor m = new Motor(100);
    Pkw pkw1 = new Pkw(1983, new Motor(50));
    Pkw pkw2 = new Pkw(2017, m);
    
    pkw1.getMotor().tune(500);
    
    System.out.println("Leistung Pkw1: " + pkw1.getMotor().getPs());
    System.out.println("Leistung Pkw2: " + pkw2.getMotor().getPs());
    
    Parkhaus ph = Parkhaus.getInstance();
    
    try {
      ph.pop();
    } catch (UnderflowException e) {}
    
    try {
      ph.push(pkw1);
      ph.push(pkw2);
      ph.push(new Pkw(2011, m));
      ph.push(new Pkw(2007, m));
      ph.push(new Pkw(1800, m));
    } catch (OverflowException e) {}
    
    try {
      System.out.println("Baujahr des letzten Pkws im Parkhaus: " + ph.pop().getBaujahr());
    } catch (UnderflowException e) {}
  }
}
