package info3.vl.singleton;
public class Pkw {

  private String plate;
  private int baujahr;
  private Motor motor;

  public Pkw(int bj, Motor m) {
    motor = m;
    baujahr = bj;
  }

  public String getPlate() {
    return plate;
  }

  public int getBaujahr() {
    return baujahr;
  }
  
  public Motor getMotor() {
    return motor;
  }
  
  public void setPlate(String kz) {
    plate = kz;
  }
  
  public boolean isOldtimer() {
    if((2018 - baujahr) >= 30)
      return true;
    else
      return false;
  }
}
