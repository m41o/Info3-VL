package info3.vl.singleton;

public class Motor {
  
  private int ps;

  public Motor(int p) {
    ps = p;
  }

  public int getPs() {
    return ps;
  }
  
  public void tune(int p) {
    ps = p;
  }
}
