package info3.vl.singleton;

public class Parkhaus {
  
  private static Parkhaus ph = null; // einzige Instanz der Parkhaus-Klasse
  private final static int MAX = 4; // Anzahl an Parkplaetzen
  private Pkw[] pkws = new Pkw[MAX];
  private int top = 0;
  
  private Parkhaus() {}
  
  public static Parkhaus getInstance() {
    if (ph==null)
      ph = new Parkhaus();
    
    return ph;
  }
  
  public Pkw pop() throws UnderflowException {
    if (top == 0) {
      throw new UnderflowException();
    }
    
    return pkws[--top];
  }
  
  public void push(Pkw pkw) throws OverflowException {
    if (top == MAX)
      throw new OverflowException();
    
    pkws[top++] = pkw;
  }
}
