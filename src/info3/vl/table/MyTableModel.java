package info3.vl.table;

import javax.swing.table.AbstractTableModel;

public class MyTableModel extends AbstractTableModel {

  private int[][] array = {{1, 4}, {2, 3}, {3, 2}, {4, 1}};
  
  @Override
  public int getColumnCount() {
    // TODO Auto-generated method stub
    return 2;
  }

  @Override
  public int getRowCount() {
    // TODO Auto-generated method stub
    return 4;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return array[rowIndex][columnIndex];
  }

  public void setValue(int value, int row) {
    array[row][1] = value;
  }

}
