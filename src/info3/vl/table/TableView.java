package info3.vl.table;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

public class TableView extends JFrame {
  private JTable table = null;
  private JButton button = new JButton("Change");
  private JTextField textField = new JTextField();
  private JPanel southPanel = new JPanel();
  private MyTableModel model = null;
  
  public TableView(MyTableModel model) {
    this.model = model;
    table = new JTable(this.model);
    this.model.addTableModelListener(table);
    add(table, BorderLayout.CENTER);
    config();
    southPanel.add(textField);
    southPanel.add(button);
    add(southPanel, BorderLayout.SOUTH);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }
  
  private void config() {
    button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int value = Integer.parseInt(textField.getText());
        model.setValue(value, table.getSelectedRow());
        model.fireTableDataChanged();
      }
    });
    
    textField.setColumns(1);
  }
}
